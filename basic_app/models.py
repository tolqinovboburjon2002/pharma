from typing import Text
from django.db import models
from django.db.models.fields import CharField, TextField
from django.db.models.fields.files import ImageField


# Create your models here.

class Dorilar(models.Model):
    dori_nomi = models.CharField(max_length=50)
    dori_narxi = models.CharField(max_length=10, blank=True)
    dori_soni = models.CharField(max_length=10)
    img = models.ImageField(upload_to='images/')
    is_sale = models.BooleanField(default=True)

    def __str__(self):
        return self.dori_nomi


class NewProduct(models.Model):
    name = models.CharField(max_length=100)
    price1 = models.CharField(max_length=100, blank=True)
    price2 = models.CharField(max_length=100)
    is_sale = models.BooleanField(default=True)
    image = models.ImageField(upload_to='images/')

    def __str__(self):
        return self.name


class Testlovchilar(models.Model):
    images = models.ImageField(upload_to='images/')
    name = models.CharField(max_length=100)
    desc = models.TextField()

    def __str__(self):
        return self.name


class Kament(models.Model):
    text = models.TextField()

    def __str__(self):
        return self.text


class GetInTouch(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField()
    subject = models.CharField(max_length=200)
    description = models.TextField()

    def __str__(self):
        return self.subject


class Shop(models.Model):
    image = models.ImageField(upload_to='images/')
    name = models.CharField(max_length=50)
    desc = models.TextField()
    price1 = models.CharField(max_length=50, blank=True)
    price2 = models.CharField(max_length=50)
    is_sale = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Character(models.Model):
    material = models.CharField(max_length=80)
    desc = models.TextField()
    pack = models.CharField(max_length=100)

    def __str__(self):
        return self.material


class Characters(models.Model):
    specific = models.CharField(max_length=150)
    things = models.CharField(max_length=150)

    def __str__(self):
        return self.things


# class FormModel(models.Model):
#     username=models.CharField(max_length=50)
#     email=models.EmailField()
# 
#     def __str__(self):
#         return self.username


#
# def __str__(self):
#     return self.name


class Jamoa(models.Model):
    name = CharField(max_length=60)
    image = models.ImageField(upload_to='images/')
    des = models.TextField()

    def __str__(self):
        return self.name


# class Jamoa(models.Model):
#     name = CharField(max_length=60)
#     image = models.ImageField('upload_to=image/')
#     des = models.TextField()

#     def __str__(self):
#         return self.name


class YetkazibBerishBepul(models.Model):
    title = models.CharField(max_length=50, default=None)
    image = models.ImageField(upload_to='images/', default=None)
    des = models.TextField()

    def __str__(self):
        return self.title


class BepulQaytarish(models.Model):
    title = models.CharField(max_length=50, default=None)
    image = models.ImageField(upload_to='images/', default=None)
    des = models.TextField()

    def __str__(self):
        return self.title


class MijozniQollash(models.Model):
    title = models.CharField(max_length=50, default=None)
    image = models.ImageField(upload_to='images/', default=None)
    des = models.TextField()

    def __str__(self):
        return self.title


class Team(models.Model):
    name = models.CharField(max_length=50)
    job = models.CharField(max_length=60)
    description = models.TextField()
    img = models.ImageField(upload_to='images/')

    def __str__(self):
        return self.name


class Office(models.Model):
    title = models.CharField(max_length=50)
    text = models.TextField()

    def __str__(self):
        return self.title


class Komp(models.Model):
    title = models.CharField(max_length=50)
    text = models.TextField()

    def __str__(self):
        return self.title


<<<<<<< HEAD
class Season(models.Model):
    buy_sell = models.CharField(max_length=60)
    shipping = models.CharField(max_length=60)
    amet = models.TextField()
    description = models.TextField()

=======
class Colour(models.Model):
    buy_sell=models.CharField(max_length=60)
    shipping=models.CharField(max_length=60)
    amet=models.TextField()
    description=models.TextField()
    
>>>>>>> ea92c42d9bc0a2845c5d19bfbae526324ca59c31
    def __str__(self):
        return self.buy_sell
