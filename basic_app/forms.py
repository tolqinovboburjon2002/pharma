from django import forms

from basic_app import models


class GetInTouchForm(forms.ModelForm):
    class Meta:
        model = models.GetInTouch
        fields = '__all__'

        widgets = {
            'first_name': forms.TextInput(attrs={'id': 'c_fname', 'class': 'form-control','pleaceholder': 'Your Firstname'})

        }
